
from templatebase import Template

class ggSpellTemplate(Template):
    def get_files(self):
        return [
            'ggSpell.js'
        ]

    def generate(self):
        name = self.prompt("Name: ", "Default Spell Name")
        desc = self.prompt("Description: ", "A Spell that does spelly things")
        self.set("name", name)
        self.set("description", desc)
        self.set("type", name[0].lower() + name.replace(" ","")[1:])

        filename_ggSpell = "spell" + self.get("name").replace(" ","")
        self.rename('ggSpell.js', filename_ggSpell + ".js")
