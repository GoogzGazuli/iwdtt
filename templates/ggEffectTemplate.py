
from templatebase import Template

class ggEffectTemplate(Template):
    def get_files(self):
        return [
            'ggEffect.js'
        ]

    def generate(self):
        name = self.prompt("Name: ", "Default Effect Name")
        self.set("type", name[0].lower() + name.replace(" ","")[1:])

        filename_ggEffect = "effect" + name.replace(" ","")
        self.rename('ggEffect.js', filename_ggEffect + ".js")
