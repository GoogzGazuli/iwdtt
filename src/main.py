import click
import os
import importlib

basedir = os.path.dirname(os.path.realpath(__file__))+'/../'
currdir = os.getcwd()+'/'

@click.group()
def cli():
    pass

@cli.command()
@click.option('--name','-n',default="Default Spell Name")
@click.argument('template')
def create(name, template):
    templates = get_templates()
    if template not in templates:
        click.echo("No template found for '" + template + "'")
        return
    
    # Load template
    template = importlib.import_module("."+template+"Template", 'templates').__dict__[template+"Template"]()
    # Get template data
    t_info = template.get_template()
    t_data = t_info['data']
    t_files = t_info['files']
    # Load files and replace variables
    for f in t_files:
        rf = open(basedir+'templates/files/'+f[0], 'r')
        buff = rf.read()
        rf.close()
        for key in t_data:
            buff = buff.replace("%%"+key+"%%", t_data[key])
        wf = open(currdir+f[1], 'w')
        wf.write(buff)
        wf.close()
    # Create new files and end

    '''
    rf = open(basedir+'templates/'+template+'.tiwd', 'r')
    buff = rf.read()
    rf.close()
    head, *tail = name.split()
    l = [head.lower()] + tail
    template = None
    filename = "spell" + name.replace(' ','')
    t = "".join(l).replace(' ','')
    print(filename)
    wf = open(currdir+filename+'.js', 'w')
    wf.write(buff)
    wf.close()
    '''

@cli.command()
def delete():
    click.echo("DELETE")

@cli.command()
def config():
    click.echo("CONFIG")

@cli.command()
def switch():
    click.echo("SWITCH")

@cli.command()
def list():
    templates = get_templates()
    for template in templates:
        print(template)
    
def get_templates():
    files = os.listdir(basedir+"templates")
    templates = []
    for template in files:
        if "Template.py" in template:
            templates.append(template.replace("Template.py", ""))
    return templates


def main():
    cli()

if __name__ == '__main__':
    main()
