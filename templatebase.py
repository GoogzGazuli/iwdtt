
class Template:
    def __init__(self):
        self.data = {}
        self.files = {}

    def get_files(self):
        pass

    def _generate(self):
        files = self.get_files()
        for f in files:
            self.files[f] = f
        self.generate()

    def set(self, name, value):
        self.data[name] = value

    def get(self, name):
        if name in self.data:
            return self.data[name]
    
    def rename(self, filename, new_filename):
        if filename in self.files:
            self.files[filename] = new_filename

    def prompt(self, prompt, default):
        value = input(prompt)
        if value == "":
            return default
        return value

    def get_template(self):
        self._generate()
        files = []
        for f in self.files:
            files.append((f,self.files[f]))
        return {
            "data": self.data,
            "files": files
        }
